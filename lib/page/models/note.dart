import 'package:flutter/material.dart';

class Note {
  String _title;
  String _content;
  Color _warnaNote;
  Color _warnaText;

  Note(
      {required String title,
      required String content,
      required Color warnaNote,
      required Color warnaText})
      : _title = title,
        _content = content,
        _warnaNote = warnaNote,
        _warnaText = warnaText;

  String get title => _title;
  set title(String newTitle) => _title = newTitle;

  String get content => _content;
  set content(String newContent) => _content = newContent;

  Color get warnaNote => _warnaNote;
  set warnaNote(Color newWarnaNote) => _warnaNote = newWarnaNote;

  Color get warnaText => _warnaText;
  set warnaText(Color newWarnaText) => _warnaText = newWarnaText;
}
