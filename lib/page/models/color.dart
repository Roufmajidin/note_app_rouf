import 'package:flutter/material.dart';

class ColorData {
  final Color color;
  final String title;

  ColorData({required this.color, required this.title});
}
