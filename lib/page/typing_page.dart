import 'package:flutter/material.dart';
import 'package:note_apps/main.dart';

import 'models/color.dart';
import 'models/note.dart';

class TypingScreen extends StatefulWidget {
  final List<Note> notes;
  // stataus page (edit atau tambah data)
  var status;
  var editIndex;

  TypingScreen(
      {super.key,
      required this.notes,
      required this.status,
      this.editIndex = -1});

  @override
  State<TypingScreen> createState() => _TypingScreenState();
}

class _TypingScreenState extends State<TypingScreen> {
  // fungsi ubah tema
  final List<ColorData> _listWarna = [
    ColorData(color: Colors.red, title: 'Merah'),
    ColorData(color: Colors.orange, title: 'Oranye'),
    ColorData(color: Colors.green, title: 'Hijau'),
    ColorData(color: Colors.brown, title: 'Cokelat'),
    ColorData(color: Colors.white, title: 'none'),
  ];
  Color _selectedColor = Colors.white;

  // ubah style texeditingcontroller jika backgorund gelap

  Color _selectedColorText = Colors.black;
  bool _isEditing = false;
  // list catatan (models ada folder /lib/models)
  // List<Note> _noteList = [];
  // cek apakah edit atau tambah data
  @override
  void initState() {
    super.initState();

    if (widget.status == "editData" &&
        widget.editIndex >= 0 &&
        widget.editIndex < widget.notes.length) {
      // Isi nilai controller dengan data yang akan diedit
      _titleController.text = widget.notes[widget.editIndex].title;
      _isiController.text = widget.notes[widget.editIndex].content;
      _selectedColor = widget.notes[widget.editIndex].warnaNote;
      _selectedColorText = widget.notes[widget.editIndex].warnaText;
    }
  }

  void ubahWarna() {
    showModalBottomSheet<void>(
      context: context,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      builder: (BuildContext context) {
        return Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(40.0),
              topRight: Radius.circular(40.0),
            ),
          ),
          padding: EdgeInsets.all(16.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              ListTile(
                title: Text('Pilih Warna'),
              ),
              ListView.builder(
                shrinkWrap: true,
                itemCount: _listWarna.length,
                itemBuilder: (BuildContext context, int index) {
                  return ListTile(
                    title: Text(_listWarna[index].title),
                    onTap: () {
                      // validasi jika warna gelap
                      _selectedColor = _listWarna[index].color;

                      if (_selectedColor == Colors.red ||
                          _selectedColor == Colors.brown ||
                          _selectedColor == Colors.green) {
                        setState(() {
                          _selectedColor = _listWarna[index].color;
                          _selectedColorText = Colors.white;
                          Navigator.pop(context);
                        });
                      } else {
                        setState(() {
                          _selectedColor = _listWarna[index].color;
                          _selectedColorText = Colors.black;
                        });
                        Navigator.pop(context);
                        setState(() {});
                      }
                    },
                    leading: Container(
                      decoration: BoxDecoration(
                          color: _listWarna[index].color,
                          borderRadius: BorderRadius.circular(50)),
                      width: 30,
                      height: 30,
                    ),
                  );
                },
              ),
            ],
          ),
        );
      },
    );
  }

// fungsi utk simpan catatan kedalam listt
  void simpanCatatan() {
    String title = _titleController.text;
    String noteText = _isiController.text;

    // logika jika terdapat texteditingcontroller yang kosong
    if (title.isNotEmpty && noteText.isNotEmpty) {
      Note newNote = Note(
          title: title,
          content: noteText,
          warnaNote: _selectedColor,
          warnaText: _selectedColorText);
      if (widget.status == "editData" &&
          widget.editIndex >= 0 &&
          widget.editIndex < widget.notes.length) {
        // Update data
        setState(() {
          widget.notes[widget.editIndex] = newNote;
          _showSnackBar("Catatan berhasil Diedit");
        });
      } else {
        // tambah data ketika statusya adalah tambahaData
        setState(() {
          widget.notes.add(newNote);
          _showSnackBar("Catatan berhasil disimpan");
        });
      }
      // clear controller
      _titleController.clear();
      _isiController.clear();
      // kalo selesai fungsi ini, maka kembali ke halaman note
      // Navigate to HomePage
      // Navigator.pop(context, );
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => MyHomePage(notes: widget.notes),
        ),
      );
    } else {
      _showSnackBar("Judul dan isi catatan tidak boleh kosong.");
    }
  }

  void _showSnackBar(String message) {
    ScaffoldMessenger.of(context)
        .showSnackBar(SnackBar(content: Text(message)));
  }

  // variabel controller
  TextEditingController _titleController = TextEditingController();
  TextEditingController _isiController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.tealAccent,
        title:
            // cek status page adalah edit atau tambah data
            Text(widget.status == "tambahData" ? "Create Note" : "Edit Note"),
      ),
      backgroundColor: _selectedColor,
      body: Padding(
        padding: EdgeInsets.all(16.0),
        child: Column(
          children: [
            TextFormField(
              controller: _titleController,
              //  border none. biar ilang border defaultnya
              decoration: InputDecoration(
                hintText: 'Title',
                border: InputBorder.none,
              ),
              style: TextStyle(
                  fontWeight: FontWeight.bold, color: _selectedColorText),
            ),
            TextFormField(
              controller: _isiController,
              //  border none. biar ilang border defaultnya
              decoration: InputDecoration(
                hintText: 'Write something here',
                border: InputBorder.none,
              ),
              style: TextStyle(
                  fontWeight: FontWeight.w300, color: _selectedColorText),

              maxLines: 10,
            ),
          ],
        ),
      ),
      // tombol save
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          FloatingActionButton(
            heroTag: "button1",
            backgroundColor: Colors.tealAccent,
            onPressed: () {
              // panggill fungsi ubah tema
              ubahWarna();
            },
            tooltip: 'Ubah Tema Note',
            child: const Icon(Icons.color_lens),
          ),
          const SizedBox(
            height: 20,
          ),
          FloatingActionButton(
            heroTag: "button2",
            backgroundColor: Colors.tealAccent,
            onPressed: () {
              setState(() {
                simpanCatatan();
              });
            },
            tooltip: 'Save',
            child: const Icon(Icons.save),
          ),
        ],
      ),
    );
  }
  // fungsi ubah tema
}
