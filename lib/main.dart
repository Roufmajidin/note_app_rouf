import 'package:flutter/material.dart';
import 'package:note_apps/page/models/note.dart';
import 'package:note_apps/page/typing_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Rouf Majidin',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      debugShowCheckedModeBanner: false,
      // kalau awalan. list notes nya kosong dlu
      home: const MyHomePage(notes: []),
    );
  }
}

class MyHomePage extends StatefulWidget {
  final List<Note> notes;
  const MyHomePage({super.key, required this.notes});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  _MyHomePageState();
// variabel gridview true dan false
  bool _isGrid = false;
  // fungsi dlete data
  void _deleteData(int index) {
    final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
    if (index >= 0 && index < widget.notes.length) {
      setState(() {
        widget.notes.removeAt(index);
      });
      _showSnackBar("Catatan berhasil Dihapus");
    }
  }

  // fungsi utk menampilakn snackbar
  void _showSnackBar(String message) {
    ScaffoldMessenger.of(context)
        .showSnackBar(SnackBar(content: Text(message)));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.tealAccent,
        title: Text("Notes"),
        actions: [
          if (widget.notes.isNotEmpty)
            IconButton(
              onPressed: () {
                setState(() {
                  _isGrid = !_isGrid;
                });
              },
              icon: Icon(_isGrid ? Icons.view_list : Icons.dashboard),
            ),
        ],
      ),
      body: Padding(
          padding: const EdgeInsets.all(18.0),
          // kalo variabel _isGrid adalah false maka bentuk list nya adalah ListView
          // kalo true maka GridView widget
          child: _isGrid == false
              ? ListView.builder(
                  physics: BouncingScrollPhysics(),
                  scrollDirection: Axis.vertical,
                  itemCount: widget.notes.length,
                  itemBuilder: (BuildContext context, int index) {
                    return InkWell(
                      onLongPress: () {
                        print("ok");
                        _showModalBottomSheet(index);
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                              color: widget.notes[index].warnaNote,
                              borderRadius: BorderRadius.circular(12)),
                          child: ListTile(
                            title: Text(widget.notes[index].title),
                            subtitle: Text(widget.notes[index].content),
                          ),
                        ),
                      ),
                    );
                  },
                )
              : GridView.builder(
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                  ),
                  itemCount: widget.notes.length,
                  itemBuilder: (context, index) {
                    final note = widget.notes[index];
                    return GestureDetector(
                      // Edit data
                      onLongPress: () {
                        _showModalBottomSheet(index);
                      },
                      child: Container(
                        width: 120,
                        height: 120,
                        decoration: BoxDecoration(
                          color: note.warnaNote,
                          borderRadius: BorderRadius.circular(12),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              note.title,
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: note.warnaText,
                              ),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                            ),
                            const SizedBox(height: 8),
                            Text(
                              note.content,
                              style: TextStyle(
                                fontWeight: FontWeight.normal,
                                color: note.warnaText,
                              ),
                              maxLines: 3,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                )),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.tealAccent,
        onPressed: () {
          // ke halaman baru yatu TypingPage
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => TypingScreen(
                      notes: widget.notes,
                      status: "tambahData",
                    )),
          );
        },
        tooltip: 'Halaman Types',
        child: const Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  // modal edit
  void _showModalBottomSheet(int index) {
    showModalBottomSheet<void>(
      context: context,

      // clipBehavior: Clip.antiAliasWithSaveLayer,
      builder: (BuildContext context) {
        return Container(
          width: MediaQuery.of(context).size.width,
          height: 100,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(40.0),
              topRight: Radius.circular(40.0),
            ),
          ),
          padding: const EdgeInsets.all(16.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SizedBox(width: 250, child: Text('Actions')),
              Container(
                decoration: BoxDecoration(
                  color: Colors.grey.shade200,
                  borderRadius: BorderRadius.circular(12.0),
                ),
                child: IconButton(
                  onPressed: () {
                    // panggil fungsi delete data , semangat
                    _deleteData(index);
                    // tutup buttomsheet
                    Navigator.pop(context);
                  },
                  icon: Icon(Icons.delete),
                  color: Colors.black, // Warna ikon
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  color: Colors.grey.shade200,
                  borderRadius: BorderRadius.circular(12.0),
                ),
                child: IconButton(
                  onPressed: () {
                    // ke halaman edit
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => TypingScreen(
                                notes: widget.notes,
                                status: "editData",
                                editIndex: index,
                              )),
                    );
                  },
                  icon: Icon(Icons.edit),
                  color: Colors.black, // Warna ikon
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
